DS=mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.AOD.e3668_s3126_r9364_r9315
TAG=v2


pathena --trf "Reco_tf.py --preExec 'from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-40\"' --inputAODFile %IN --outputDAODFile pool.root --reductionConf FTAG1"\
        --noEmail\
        --inDS ${DS}\
        --outDS user.dguest.likes_flip_taggers.${TAG}

pathena --trf "Reco_tf.py --preExec 'from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-40\"; BTaggingFlags.MultivariateFlipTagManagerAuxBranches = [\"rnnipneg_pu\", \"rnnipneg_pc\", \"rnnipneg_pb\", \"rnnipneg_ptau\"]' --inputAODFile %IN --outputDAODFile pool.root --reductionConf FTAG1"\
        --noEmail\
        --inDS ${DS}\
        --outDS user.dguest.likes_neg_taggers.${TAG}
